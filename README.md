# RSSM (RoSSoM)

This is the Arduino C++ code for the Running Shoe Stiffness Measurement, made for the graduation project of Lisanne Sevas.

## Compile

Clone this repository with the `--recursive` flag, or run `git submodule update --init` afterwards.

 1. Open the `*.ino` file in Arduino IDE (or VS Code with the Arduino extension)
 2. Go to 'Tools' and open the 'Library Manager'. Install the 'HX177 Arduino Library' library (see [github](https://github.com/RobTillaart/HX711))
 3. Set the board to 'Arduino Uno'
 4. Hit verify or upload

## Hardware

The set-up uses a RedBoard, which can be programmed exactly like an Arduino Uno.

The HX177 load cell amplifier is used. The interface to Arduino is not fully understood, it uses a data and clock line, but it doesn't seem I2C. The library linked above handles all communication details.
