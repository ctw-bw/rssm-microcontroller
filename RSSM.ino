#include <HX711.h>
#include "src/uscope/scope.h"

// Sample rate (seconds)
const float TS = 1.0f / 200.0f; // 1 / Hz

// This value is obtained using the SparkFun_HX711_Calibration sketch
const float calibration_factor = 7050.0f;

// Communication pins (_not_ real I2C!)
#define PIN_DOUT 3
#define PIN_CLK 2

// Analog in for the potmeter
#define PIN_POTMETER A0

HX711 load;

// Disable SCOPE to use serial for debugging instead
#define SCOPE true

#if SCOPE
Scope scope(2); // Cell readout and potmeter
#endif

/**
 * Initialization function
 * 
 * Run only once.
 */
void setup() {

    Serial.begin(115200);

#if !SCOPE
    Serial.println("Running Shoe Tester...");
#endif

    // Initialize load cell
    load.begin(PIN_DOUT, PIN_CLK);

    load.set_scale(calibration_factor);
    load.tare();
    // Assuming there is no weight on the cell at start up,
    // reset the cell to 0
}

/**
 * Main loop function
 * 
 * Function calls are timed to TS.
 */
void loop_main() {

    float force = load.get_units(); // Get load cell readout in converted units

    int pot_int = analogRead(PIN_POTMETER); // Get potmeter value as integer from 0 to 1024

    float pot = (float)(pot_int) / 1024.0f;

    float angle = pot * 270.0f + 0.0f;

#if SCOPE
    scope.set(0, angle);
    scope.set(1, force);
    scope.send();
#endif
}

/**
 * Default loop function
 * 
 * Repeated continuously. Use to call the timed loop function.
 */
void loop() {

    static unsigned long last_frame = 0; // Keep track of frame time

    // Hang until the previous execution time plus wait time
    // equals a sample time
    while (micros() - last_frame < TS) {}

    last_frame = micros(); // Remember the start time of the next frame
    
    loop_main();
}
